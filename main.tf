module "do_k8s_cluster" {
  source             = "./modules/do_k8s_cluster"
  digitalocean_token = var.digitalocean_token
  k8s_token          = var.k8s_token
  prefix             = var.prefix
  do_region          = var.do_region
  do_image           = var.do_image
  do_node_count      = var.node_count
  do_node_size       = var.do_node_size
  node_volume_size   = var.node_volume_size
  node_volume_count  = var.node_volume_count
  do_controller_size = var.do_controller_size
  cluster_name       = var.cluster_name
  do_ssh_keys        = var.do_ssh_keys
  do_ssh_private_key = var.do_ssh_private_key
  kubernetes_version = var.kubernetes_version
  openebs_ready      = var.openebs_ready
}

module "do_rancher_cluster" {
  source                         = "./modules/do_rancher_cluster"
  digitalocean_token             = var.digitalocean_token
  prefix                         = var.prefix
  rancher_version                = var.rancher_version
  count_agent_all_nodes          = var.count_agent_all_nodes
  count_agent_etcd_nodes         = var.count_agent_etcd_nodes
  count_agent_controlplane_nodes = var.count_agent_controlplane_nodes
  count_agent_worker_nodes       = var.node_count
  node_volume_count              = var.node_volume_count
  node_volume_size               = var.node_volume_size
  admin_password                 = var.rancher_admin_password
  cluster_name                   = var.cluster_name
  do_region                      = var.do_region
  do_rancher_server_size         = var.do_rancher_server_size
  do_rancher_all_size            = var.do_rancher_all_size
  do_rancher_controlplane_size   = var.do_rancher_controlplane_size
  do_rancher_worker_size         = var.do_rancher_worker_size
  do_rancher_etcd_size           = var.do_rancher_etcd_size
  docker_version_server          = var.docker_version_server
  docker_version_agent           = var.docker_version_agent
  do_ssh_keys                    = var.do_ssh_keys
  openebs_ready                  = var.openebs_ready
}

module "gce_k8s_cluster" {
  source                   = "./modules/gce_k8s_cluster"
  cluster_name             = var.cluster_name
  kubeconfig_path          = var.kubeconfig_path
  gcp_credentials_file     = var.gcp_credentials_file
  gcp_project_id           = var.gcp_project_id
  gcp_region               = var.gcp_region
  gcp_zone                 = var.gcp_zone
  gcp_k8s_controller_size  = var.gcp_k8s_controller_size
  gcp_k8s_controller_image = var.gcp_k8s_controller_image
  gcp_k8s_node_size        = var.gcp_k8s_node_size
  gcp_k8s_node_image       = var.gcp_k8s_node_image
  node_volume_count        = var.node_volume_count
  node_volume_size         = var.node_volume_size
  disk_interface           = var.disk_interface
  gcp_boot_disk_size       = var.gcp_boot_disk_size
  node_count               = var.node_count
  kubernetes_version       = var.kubernetes_version
  k8s_token                = var.k8s_token
  ssh_user                 = var.ssh_user
  ssh_key                  = var.ssh_key
  openebs_ready            = var.openebs_ready
}

module "gce_rancher_cluster" {
  source                         = "./modules/gce_rancher_cluster"
  gcp_credentials_file           = var.gcp_credentials_file
  gcp_project_id                 = var.gcp_project_id
  gcp_region                     = var.gcp_region
  gcp_zone                       = var.gcp_zone
  prefix                         = var.prefix
  rancher_version                = var.rancher_version
  gcp_rancher_image              = var.gcp_rancher_image
  count_agent_all_nodes          = var.count_agent_all_nodes
  count_agent_etcd_nodes         = var.count_agent_etcd_nodes
  count_agent_controlplane_nodes = var.count_agent_controlplane_nodes
  count_agent_worker_nodes       = var.node_count
  node_volume_count              = var.node_volume_count
  node_volume_size               = var.node_volume_size
  admin_password                 = var.rancher_admin_password
  cluster_name                   = var.cluster_name
  gcp_rancher_server_size        = var.gcp_rancher_server_size
  gcp_rancher_all_size           = var.gcp_rancher_all_size
  gcp_rancher_controlplane_size  = var.gcp_rancher_controlplane_size
  gcp_rancher_worker_size        = var.gcp_rancher_worker_size
  gcp_rancher_etcd_size          = var.gcp_rancher_etcd_size
  docker_version_server          = var.docker_version_server
  docker_version_agent           = var.docker_version_agent
  ssh_key                        = var.ssh_key
  openebs_ready                  = var.openebs_ready
}

module "aws_rancher_cluster" {
  source                         = "./modules/aws_rancher_cluster"
  prefix                         = var.prefix
  cluster_name                   = var.cluster_name
  admin_password                 = var.rancher_admin_password
  docker_version_server          = var.docker_version_server
  docker_version_agent           = var.docker_version_agent
  rancher_version                = var.rancher_version
  aws_region                     = var.aws_region
  aws_zones                      = var.aws_zones
  aws_rancher_vpc_cidr          = var.aws_rancher_vpc_cidr
  aws_rancher_cidr_public_subnet = var.aws_rancher_cidr_public_subnet
  aws_rancher_cidr_private_subnets = var.aws_rancher_cidr_private_subnets
  aws_shared_credentials_file    = var.aws_shared_credentials_file
  aws_profile                    = var.aws_profile
  aws_rancher_server_size        = var.aws_rancher_server_size
  aws_node_volume_type           = var.aws_node_volume_type
  aws_key_name                   = var.aws_key_name
  count_agent_all_nodes          = var.count_agent_all_nodes
  count_agent_etcd_nodes         = var.count_agent_etcd_nodes
  count_agent_controlplane_nodes = var.count_agent_controlplane_nodes
  count_agent_worker_nodes       = var.node_count
  aws_rancher_all_size           = var.aws_rancher_all_size
  aws_rancher_etcd_size          = var.aws_rancher_etcd_size
  aws_rancher_controlplane_size  = var.aws_rancher_controlplane_size
  aws_rancher_worker_size        = var.aws_rancher_worker_size
  openebs_ready                  = var.openebs_ready
  node_volume_count              = var.node_volume_count
  node_volume_size               = var.node_volume_size
}

module "aws_k8s_cluster" {
  source                         = "./modules/aws_k8s_cluster"
  prefix                         = var.prefix
  cluster_name                   = var.cluster_name
  aws_region                     = var.aws_region
  aws_zones                      = var.aws_zones
  aws_k8s_vpc_cidr               = var.aws_k8s_vpc_cidr
  aws_k8s_cidr_public_subnet     = var.aws_k8s_cidr_public_subnet
  aws_k8s_cidr_private_subnets   = var.aws_k8s_cidr_private_subnets
  aws_shared_credentials_file    = var.aws_shared_credentials_file
  aws_profile                    = var.aws_profile
  aws_k8s_controller_size        = var.aws_k8s_controller_size
  aws_k8s_node_size              = var.aws_k8s_node_size
  aws_node_volume_type           = var.aws_node_volume_type
  aws_key_name                   = var.aws_key_name
  kubernetes_version             = var.kubernetes_version
  k8s_token                      = var.k8s_token
  kubeconfig_path                = var.kubeconfig_path
  ssh_key                        = var.ssh_key
  aws_node_count                 = var.node_count
  openebs_ready                  = var.openebs_ready
  node_volume_count              = var.node_volume_count
  node_volume_size               = var.node_volume_size
  k8s_wait_for_nodes             = var.k8s_wait_for_nodes
}

module "hetzner_rancher_cluster" {
  source                         = "./modules/hetzner_rancher_cluster"
  prefix                         = var.prefix
  cluster_name                   = var.cluster_name
  hcloud_token                   = var.hcloud_token
  hetzner_location                = var.hetzner_location
  admin_password                 = var.rancher_admin_password
  docker_version_server          = var.docker_version_server
  docker_version_agent           = var.docker_version_agent
  rancher_version                = var.rancher_version
  hetzner_rancherserver_type    = var.hetzner_rancherserver_type
  ssh_key                        = var.ssh_key
  ssh_user                       = var.ssh_user
  count_agent_all_nodes          = var.count_agent_all_nodes
  count_agent_etcd_nodes         = var.count_agent_etcd_nodes
  count_agent_controlplane_nodes = var.count_agent_controlplane_nodes
  count_agent_worker_nodes       = var.node_count
  hetzner_rancheragent-all_type           = var.hetzner_rancher_all_type
  hetzner_rancheragent-etcd_type          = var.hetzner_rancher_etcd_type
  hetzner_rancheragent-controlplane_type  = var.hetzner_rancher_controlplane_type
  hetzner_rancheragent-worker_type        = var.hetzner_rancher_worker_type
  node_volume_count              = var.node_volume_count
  node_volume_size               = var.node_volume_size
  ssh_key_name                   = var.ssh_key_name
  floating_ip                    = var.floating_ip
}

output "do_kubeconfig" {
  value = "${module.do_k8s_cluster.config}"
}

output "do_rancher_url" {
  value = "${module.do_rancher_cluster.rancher-url}"
}

output "gce_kubeconfig" {
  value = "${module.gce_k8s_cluster.config}"
}

output "gce_rancher_url" {
  value = "${module.gce_rancher_cluster.rancher-url}"
}

output "aws_rancher_url" {
  value = "${module.aws_rancher_cluster.rancher-url}"
}

output "aws_kubeconfig" {
  value = "${module.aws_k8s_cluster.config}"
}

output "hetzner_rancher_url" {
  value = "${module.hetzner_rancher_cluster.rancher-url}"
}