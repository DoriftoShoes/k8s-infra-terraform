provider "digitalocean" {
  token = "${var.digitalocean_token}"
}

resource "digitalocean_droplet" "rancherserver" {
  image     = "${var.do_image}"
  name      = "${var.prefix}-rancherserver"
  region    = "${var.do_region}"
  size      = "${var.do_rancher_server_size}"
  user_data = "${data.template_file.userdata_server.rendered}"
  ssh_keys  = "${var.do_ssh_keys}"
}

resource "digitalocean_volume" "rancheragent-all" {
  count                   = "${var.count_agent_all_nodes * var.node_volume_count}"
  region                  = "${var.do_region}"
  name                    = "${var.prefix}-rancheragent-${count.index}-all"
  size                    = "${var.node_volume_size}"
  description             = "Rancheragent all volume ${count.index}"
}

resource "digitalocean_droplet" "rancheragent-all" {
  count     = "${var.count_agent_all_nodes}"
  image     = "${var.do_image}"
  name      = "${var.prefix}-rancheragent-${count.index}-all"
  region    = "${var.do_region}"
  size      = "${var.do_rancher_all_size}"
  user_data = "${data.template_file.userdata_agent.rendered}"
  ssh_keys  = "${var.do_ssh_keys}"
  volume_ids = [
    for vol in slice(digitalocean_volume.rancheragent-all, (count.index * var.node_volume_count), ((count.index * var.node_volume_count) + (var.node_volume_count))):
    vol.id
  ]
}

resource "digitalocean_droplet" "rancheragent-etcd" {
  count     = "${var.count_agent_etcd_nodes}"
  image     = "${var.do_image}"
  name      = "${var.prefix}-rancheragent-${count.index}-etcd"
  region    = "${var.do_region}"
  size      = "${var.do_rancher_etcd_size}"
  user_data = "${data.template_file.userdata_agent.rendered}"
  ssh_keys  = "${var.do_ssh_keys}"
}

resource "digitalocean_droplet" "rancheragent-controlplane" {
  count     = "${var.count_agent_controlplane_nodes}"
  image     = "${var.do_image}"
  name      = "${var.prefix}-rancheragent-${count.index}-controlplane"
  region    = "${var.do_region}"
  size      = "${var.do_rancher_controlplane_size}"
  user_data = "${data.template_file.userdata_agent.rendered}"
  ssh_keys  = "${var.do_ssh_keys}"
}

resource "digitalocean_volume" "rancheragent-worker" {
  count                   = "${var.count_agent_worker_nodes * var.node_volume_count}"
  region                  = "${var.do_region}"
  name                    = "${var.prefix}-rancheragent-${count.index}-worker"
  size                    = "${var.node_volume_size}"
  description             = "Rancheragent worker volume ${count.index}"
}

resource "digitalocean_droplet" "rancheragent-worker" {
  count      = "${var.count_agent_worker_nodes}"
  image      = "${var.do_image}"
  name       = "${var.prefix}-rancheragent-${count.index}-worker"
  region     = "${var.do_region}"
  size       = "${var.do_rancher_worker_size}"
  user_data  = "${data.template_file.userdata_agent.rendered}"
  ssh_keys   = "${var.do_ssh_keys}"
  volume_ids = [
    for vol in slice(digitalocean_volume.rancheragent-worker, (count.index * var.node_volume_count), ((count.index * var.node_volume_count) + (var.node_volume_count))):
    vol.id
  ]
}

data "template_file" "userdata_server" {
  template = "${file("${path.module}/../files/rancher_userdata_server")}"

  vars = {
    admin_password        = "${var.admin_password}"
    cluster_name          = "${var.cluster_name}"
    docker_version_server = "${var.docker_version_server}"
    rancher_version       = "${var.rancher_version}"
  }
}

data "template_file" "userdata_agent" {
  template = "${file("${path.module}/../files/rancher_userdata_agent")}"

  vars = {
    cloud_provider        = "DO"
    admin_password       = "${var.admin_password}"
    cluster_name         = "${var.cluster_name}"
    docker_version_agent = "${var.docker_version_agent}"
    rancher_version      = "${var.rancher_version}"
    server_address       = "${digitalocean_droplet.rancherserver.ipv4_address}"
    openebs_ready        = "${var.openebs_ready}"
  }
}
