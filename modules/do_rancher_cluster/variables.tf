# Configure the DigitalOcean Provider
variable "digitalocean_token" {
  default = "xxx"
}

variable "prefix" {
  default = "yourname"
}

variable "rancher_version" {
  default = "latest"
}

variable "count_agent_all_nodes" {
  default = "3"
}

variable "count_agent_etcd_nodes" {
  default = "0"
}

variable "count_agent_controlplane_nodes" {
  default = "0"
}

variable "count_agent_worker_nodes" {
  default = "0"
}

variable "admin_password" {
  default = "admin"
}

variable "cluster_name" {
  default = "quickstart"
}

variable "do_region" {
  default = "ams3"
}

variable "node_count" {
    default = 3
}

variable "node_volume_count" {
    default = 2
}

variable "node_volume_size" {
    default = 100
}

variable "do_image" {
    default = "ubuntu-18-04-x64"
}

variable "do_rancher_server_size" {
  default = "s-2vcpu-4gb"
}

variable "do_rancher_all_size" {
  default = "s-2vcpu-4gb"
}

variable "do_rancher_controlplane_size" {
  default = "s-2vcpu-4gb"
}

variable "do_rancher_worker_size" {
  default = "s-2vcpu-4gb"
}

variable "do_rancher_etcd_size" {
  default = "s-2vcpu-4gb"
}

variable "docker_version_server" {
  default = "18.09"
}

variable "docker_version_agent" {
  default = "18.09"
}

variable "do_ssh_keys" {
  default = []
}

variable "openebs_ready" {
  default = 1
}