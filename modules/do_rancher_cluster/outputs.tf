output "rancher-url" {
  value = ["https://${digitalocean_droplet.rancherserver.ipv4_address}"]
}