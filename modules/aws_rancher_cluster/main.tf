provider "aws" {
  region                  = "${var.aws_region}"
  shared_credentials_file = "${var.aws_shared_credentials_file}"
  profile                 = "${var.aws_profile}"
}

resource "aws_vpc" "rancher_cluster" {
  cidr_block = "${var.aws_rancher_vpc_cidr}"

  tags = {
    Name = "vpc-${var.prefix}-${var.cluster_name}"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.rancher_cluster.id}"
  tags = {
    Name = "igw-${var.prefix}-${var.cluster_name}"
  }
}

resource "aws_eip" "gw_eip" {
  vpc      = true
  depends_on = ["aws_internet_gateway.igw"]
}

resource "aws_nat_gateway" "gw" {
  allocation_id = "${aws_eip.gw_eip.id}"
  subnet_id     = "${aws_subnet.subnet_public.id}"
  depends_on = ["aws_internet_gateway.igw"]
}

resource "aws_route_table" "rt_private" {
    vpc_id = "${aws_vpc.rancher_cluster.id}"

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = "${aws_nat_gateway.gw.id}"
    }

    tags = {
        Name = "route_table-${var.prefix}-${var.cluster_name}"
    }
}

resource "aws_subnet" "subnet_public" {
  vpc_id = "${aws_vpc.rancher_cluster.id}"
  cidr_block = "${var.aws_rancher_cidr_public_subnet}"
  map_public_ip_on_launch = "true"
  availability_zone = "${var.aws_zones.0}"
  tags = {
    Name = "public-subnet-${var.prefix}-${var.cluster_name}"
  }
}

resource "aws_subnet" "subnet_private" {
  count = "${length(var.aws_rancher_cidr_private_subnets)}"
  vpc_id = "${aws_vpc.rancher_cluster.id}"
  cidr_block = "${element(var.aws_rancher_cidr_private_subnets,count.index)}"
  availability_zone = "${element(var.aws_zones, count.index)}"
  tags = {
    Name = "private-subnet-${var.prefix}-${var.cluster_name}"
  }
}

resource "aws_route" "public" {
  route_table_id            = "${aws_vpc.rancher_cluster.main_route_table_id}"
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = "${aws_internet_gateway.igw.id}"
}

resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = "${aws_subnet.subnet_public.id}"
  route_table_id = "${aws_vpc.rancher_cluster.main_route_table_id}"
}

resource "aws_route_table_association" "rta_subnet_private" {
  count = "${length(var.aws_rancher_cidr_private_subnets)}"
  subnet_id      = "${element(aws_subnet.subnet_private.*.id,count.index)}"
  #route_table_id = "${aws_vpc.rancher_cluster.main_route_table_id}"
  route_table_id = "${aws_route_table.rt_private.id}"
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "template_cloudinit_config" "rancherserver-cloudinit" {
  part {
    content_type = "text/cloud-config"
    content      = "hostname: ${var.prefix}-rancherserver\nmanage_etc_hosts: true"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.userdata_server.rendered}"
  }
}



data "template_file" "userdata_server" {
  template = "${file("${path.module}/../files/rancher_userdata_server")}"

  vars = {
    admin_password        = "${var.admin_password}"
    cluster_name          = "${var.cluster_name}"
    docker_version_server = "${var.docker_version_server}"
    rancher_version       = "${var.rancher_version}"
  }
}

data "template_cloudinit_config" "rancheragent-all-cloudinit" {
  count = "${var.count_agent_all_nodes}"

  part {
    content_type = "text/cloud-config"
    content      = "hostname: ${var.prefix}-rancheragent-${count.index}-all\nmanage_etc_hosts: true"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.userdata_agent.rendered}"
  }
}

data "template_cloudinit_config" "rancheragent-etcd-cloudinit" {
  count = "${var.count_agent_etcd_nodes}"

  part {
    content_type = "text/cloud-config"
    content      = "hostname: ${var.prefix}-rancheragent-${count.index}-etcd\nmanage_etc_hosts: true"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.userdata_agent.rendered}"
  }
}

data "template_cloudinit_config" "rancheragent-controlplane-cloudinit" {
  count = "${var.count_agent_controlplane_nodes}"

  part {
    content_type = "text/cloud-config"
    content      = "hostname: ${var.prefix}-rancheragent-${count.index}-controlplane\nmanage_etc_hosts: true"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.userdata_agent.rendered}"
  }
}

data "template_cloudinit_config" "rancheragent-worker-cloudinit" {
  count = "${var.count_agent_worker_nodes}"

  part {
    content_type = "text/cloud-config"
    content      = "hostname: ${var.prefix}-rancheragent-${count.index}-worker\nmanage_etc_hosts: true"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.userdata_agent.rendered}"
  }
}

data "template_file" "userdata_agent" {
  template = "${file("${path.module}/../files/rancher_userdata_agent")}"

  vars = {
    admin_password       = "${var.admin_password}"
    cluster_name         = "${var.cluster_name}"
    docker_version_agent = "${var.docker_version_agent}"
    rancher_version      = "${var.rancher_version}"
    server_address       = "${aws_instance.rancherserver.private_ip}"
    openebs_ready        = "${var.openebs_ready}"
  }
}

resource "random_string" "device_name" {
    count = "${var.node_volume_count}"
    length = 1
    number = false
    upper = false
    special = false
}

resource "aws_instance" "rancherserver" {
    ami           = "${data.aws_ami.ubuntu.id}"
    instance_type = "${var.aws_rancher_server_size}"
    subnet_id = "${aws_subnet.subnet_public.id}"
    availability_zone = "${aws_subnet.subnet_public.availability_zone}"
    tags = {
        Name = "${var.prefix}-${var.cluster_name}-rancherserver"
    }
    user_data     = "${data.template_cloudinit_config.rancherserver-cloudinit.rendered}"
    key_name      = "${var.aws_key_name}"
    security_groups = ["${aws_security_group.rancherserver.id}"]
}

resource "aws_instance" "rancheragent-all" {
    count             = "${var.count_agent_all_nodes}"
    ami               = "${data.aws_ami.ubuntu.id}"
    instance_type     = "${var.aws_rancher_all_size}"
    #subnet_id = "${aws_subnet.subnet_public.id}"
    #availability_zone = "${aws_subnet.subnet_public.availability_zone}"
    subnet_id = "${element(aws_subnet.subnet_private.*.id, count.index)}"
    availability_zone = "${element(aws_subnet.subnet_private.*.availability_zone, count.index)}"
    tags = {
        Name = "${var.prefix}-${var.cluster_name}-rancheragent-${count.index}-all"
    }
    user_data         = "${element(data.template_cloudinit_config.rancheragent-all-cloudinit.*.rendered,count.index)}"
    key_name          = "${var.aws_key_name}"
    security_groups = ["${aws_security_group.rancher_cluster-main.id}","${aws_security_group.rancheragent-etcd.id}","${aws_security_group.rancheragent-controlplane.id}","${aws_security_group.rancheragent-worker.id}"]

    dynamic "ebs_block_device" {
        for_each = [for name in random_string.device_name: {
            device_name = name.result
        }]

        content {
            device_name = "/dev/xvd${ebs_block_device.value.device_name}"
            volume_size = "${var.node_volume_size}"
            volume_type = "${var.aws_node_volume_type}"
        }       
    }
}

resource "aws_instance" "rancheragent-etcd" {
    count             = "${var.count_agent_etcd_nodes}"
    ami               = "${data.aws_ami.ubuntu.id}"
    instance_type     = "${var.aws_rancher_etcd_size}"
    #subnet_id = "${aws_subnet.subnet_public.id}"
    #availability_zone = "${aws_subnet.subnet_public.availability_zone}"
    subnet_id = "${element(aws_subnet.subnet_private.*.id, count.index)}"
    availability_zone = "${element(aws_subnet.subnet_private.*.availability_zone, count.index)}"
    tags = {
        Name = "${var.prefix}-${var.cluster_name}-rancheragent-${count.index}-etcd"
    }
    user_data         = "${element(data.template_cloudinit_config.rancheragent-etcd-cloudinit.*.rendered,count.index)}"
    key_name          = "${var.aws_key_name}"
    security_groups = ["${aws_security_group.rancher_cluster-main.id}","${aws_security_group.rancheragent-etcd.id}"]
}

resource "aws_instance" "rancheragent-controlplane" {
    count             = "${var.count_agent_controlplane_nodes}"
    ami               = "${data.aws_ami.ubuntu.id}"
    instance_type     = "${var.aws_rancher_controlplane_size}"
    #subnet_id = "${aws_subnet.subnet_public.id}"
    #availability_zone = "${aws_subnet.subnet_public.availability_zone}"
    subnet_id = "${element(aws_subnet.subnet_private.*.id, count.index)}"
    availability_zone = "${element(aws_subnet.subnet_private.*.availability_zone, count.index)}"
    tags = {
        Name = "${var.prefix}-${var.cluster_name}-rancheragent-${count.index}-controlplane"
    }
    user_data         = "${element(data.template_cloudinit_config.rancheragent-controlplane-cloudinit.*.rendered,count.index)}"
    key_name          = "${var.aws_key_name}"
    security_groups = ["${aws_security_group.rancher_cluster-main.id}","${aws_security_group.rancheragent-controlplane.id}"]
}

resource "aws_instance" "rancheragent-worker" {
    count             = "${var.count_agent_worker_nodes}"
    ami               = "${data.aws_ami.ubuntu.id}"
    instance_type     = "${var.aws_rancher_worker_size}"
    #subnet_id = "${aws_subnet.subnet_public.id}"
    #availability_zone = "${aws_subnet.subnet_public.availability_zone}"
    subnet_id = "${element(aws_subnet.subnet_private.*.id, count.index)}"
    availability_zone = "${element(aws_subnet.subnet_private.*.availability_zone, count.index)}"
    tags = {
        Name = "${var.prefix}-${var.cluster_name}-rancheragent-${count.index}-worker"
    }
    user_data         = "${element(data.template_cloudinit_config.rancheragent-worker-cloudinit.*.rendered,count.index)}"
    key_name          = "${var.aws_key_name}"
    security_groups = ["${aws_security_group.rancher_cluster-main.id}","${aws_security_group.rancheragent-worker.id}"]

    dynamic "ebs_block_device" {
        for_each = [for name in random_string.device_name: {
            device_name = name.result
        }]

        content {
            device_name = "/dev/xvd${ebs_block_device.value.device_name}"
            volume_size = "${var.node_volume_size}"
            volume_type = "${var.aws_node_volume_type}"
        }       
    }
}

resource "aws_security_group" "rancher_cluster-main" {
  name = "${var.prefix}-${var.cluster_name}-rancher_cluster-main"
  description = "main SG"
  vpc_id = "${aws_vpc.rancher_cluster.id}"

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "allow_all_internally" {
  type            = "ingress"
  from_port       = 0
  to_port         = 65535
  protocol        = "-1"
  source_security_group_id = "${aws_security_group.rancher_cluster-main.id}"
  security_group_id = "${aws_security_group.rancher_cluster-main.id}"
}

resource "aws_security_group" "rancherserver" {
  name        = "${var.prefix}-${var.cluster_name}-rancherserver"
  description = "rancherserver"
  vpc_id = "${aws_vpc.rancher_cluster.id}"

  ingress {
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "rancheragent-worker" {
  name        = "${var.prefix}-${var.cluster_name}-rancheragent-worker"
  description = "rancheragent-worker"
  vpc_id = "${aws_vpc.rancher_cluster.id}"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8472
    to_port     = 8472
    protocol    = "udp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 10250
    to_port     = 10250
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9099
    to_port     = 9099
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 10254
    to_port     = 10254
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "rancheragent-etcd" {
  name        = "${var.prefix}-${var.cluster_name}-rancheragent-etcd"
  description = "rancheragent-etcd"
  vpc_id = "${aws_vpc.rancher_cluster.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 2376
    to_port     = 2376
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 2379
    to_port     = 2380
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8472
    to_port     = 8472
    protocol    = "udp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 10250
    to_port     = 10250
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "rancheragent-controlplane" {
  name        = "${var.prefix}-${var.cluster_name}-rancheragent-controlplane"
  description = "rancheragent-controlplane"
  vpc_id = "${aws_vpc.rancher_cluster.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 2376
    to_port     = 2376
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9099
    to_port     = 9099
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 10254
    to_port     = 10254
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}