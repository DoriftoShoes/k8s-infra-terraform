#!/bin/bash

function check_deps() {
  apt-get install python-pip > /dev/null 2>&1
  pip install -U pip > /dev/null 2>&1
  pip install yq > /dev/null 2>&1
  test -f $(which jq) || error_exit "jq command not detected in path, please install it"
}
function parse_input() {
  eval "$(jq -r '@sh "export HOST=\(.controller) > /dev/null 2>&1; export USER=\(.user) > /dev/null 2>&1; export SSHKEY=\(.sshkey) > /dev/null 2>&1"')"
  if [[ -z "${HOST}" ]]; then export HOST=none; fi
}
function return_config() {
  ssh -i ${SSHKEY} -o UserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oLogLevel=QUIET ${USER}@$HOST -q "sudo apt -qq -y install python-pip jq > /dev/null 2>&1;sudo pip install --quiet yq; sudo cat /etc/kubernetes/admin.conf | yq -c '.'" >& /tmp/kubeconfig
  until grep apiVersion /tmp/kubeconfig > /dev/null
  do 
      ssh -i ${SSHKEY} -o UserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oLogLevel=QUIET ${USER}@$HOST -q "sudo apt -qq -y install python-pip jq > /dev/null 2>&1;sudo pip install --quiet yq; sudo cat /etc/kubernetes/admin.conf | yq -c '.'" >& /tmp/kubeconfig
      sleep 10
  done
  CONFIG=$(cat /tmp/kubeconfig)
  rm /tmp/kubeconfig
  jq -c -n --arg config "$CONFIG" '{"kubeconfig":$config}'
}

check_deps
parse_input
return_config