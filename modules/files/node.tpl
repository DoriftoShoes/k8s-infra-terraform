#!/bin/bash

function install_docker() {
 apt-get update; \
 apt-get install -y docker.io && \
 cat << EOF > /etc/docker/daemon.json
 {
   "exec-opts": ["native.cgroupdriver=cgroupfs"]
 }
EOF
}

function install_kube_tools() {
  apt-get update && apt-get install -y apt-transport-https
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
  echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
  apt-get update
  apt-get install -y kubelet=${kube_version} kubeadm=${kube_version} kubectl=${kube_version}
  until systemctl --quiet status docker | grep active >/dev/null 2>&1;
  do
      echo 'waiting on docker to start...'
      sleep 5
  done
  until which kubeadm; 
  do 
      echo 'waiting on kubeadm install...';
      sleep 5; 
  done;
}

function openebs() {
  if [[ "$openebs_ready" == "1" ]]; then
    # OpenEBS iscsci fix
    apt install open-iscsi
    systemctl enable iscsid && sudo systemctl start iscsid
    modprobe iscsi_tcp

    #OpenEBS unmount disks
    for i in `ls -1 /mnt/disks`; do umount /mnt/disks/$i; done
    for i in `ls -1 /mnt`; do umount /mnt/$i; done
  else
    echo "OpenEBS not enabled"
  fi
}

openebs
install_docker
install_kube_tools