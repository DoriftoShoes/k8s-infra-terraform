variable "gcp_credentials_file" {
    default = "gcloud_credentials.json"
}

variable "gcp_project_id" {
    default = "mayadata-sample"
}

variable "cluster_name" {
}

variable "k8s_wait_for_nodes" {
    default = "true"
}

variable "gcp_region" {
    default = "us-central-1"
}
variable "gcp_zone" {
    default = "us-central1-a"
}

variable "gcp_k8s_controller_size" {
    default = ""
}

variable "gcp_k8s_controller_image" {
    default = ""
}

variable "gcp_k8s_node_size" {
    default = ""
}

variable "gcp_k8s_node_image" {
    default = ""
}

variable "gcp_boot_disk_size" {
    default = 20
}

variable "node_count" {
    default = "3"
}

variable "node_volume_count" {
    default = "2"
}

variable "node_volume_size" {
    default = "50"
}

variable "kubernetes_version" {
    default = ""
}

variable "k8s_token" {
    default = ""
}

variable "pod_network_cidr" {
    default = "10.244.0.0/16"
}

variable "apiserver_advertise_address" {
    default = "0.0.0.0"
}

variable "apiserver_fw_source_range" {
    default = "0.0.0.0/0"
}

variable "apiserver_cert_extra_sans" {
    default = [""]
    type = list(string)
}

variable "kubeconfig_path" {
    default = ""
}

variable "ssh_user" {
    default = "sampleuser"
}

variable "ssh_key" {
    default = "CHANGEME"
}

variable "disk_interface" {
    default = "SCSI"
}

variable "openebs_ready" {}