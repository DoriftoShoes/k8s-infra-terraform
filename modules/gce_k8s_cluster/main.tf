locals {
    ssh_private_key = "${element(split(".pub","${var.ssh_key}"),0)}"
    kubeconfig_path = "${var.kubeconfig_path != "" ? var.kubeconfig_path : "kubeconfig_gce_${var.gcp_project_id}_${var.cluster_name}"}"
}

provider "google" {
    credentials = "${file("${var.gcp_credentials_file}")}"
    project = "${var.gcp_project_id}"
    region = "${var.gcp_region}"
}

data "template_file" "controller" {
  template = "${file("${path.module}/../files/controller.tpl")}"

  vars = {
    kube_token         = "${var.k8s_token}"
    kube_version       = "${var.kubernetes_version}"
  }
}

resource "google_compute_instance" "controller" {
    name = "k8s-${var.cluster_name}-controller"
    machine_type = "${var.gcp_k8s_controller_size}"
    zone = "${var.gcp_zone}"
    boot_disk {
        initialize_params {
            image = "${var.gcp_k8s_controller_image}"
            size = "${var.gcp_boot_disk_size}"
        }
    }
    metadata = {
        user-data = "${data.template_file.controller.rendered}"
        sshKeys = "${var.ssh_user}:${file(var.ssh_key)}"
    }
    network_interface {
        network = "default"
        access_config {}
    }
    service_account {
        scopes = ["userinfo-email", "compute-ro", "storage-ro"]
    }

    provisioner "remote-exec" {
        connection {
            host        = "${google_compute_instance.controller.network_interface.0.access_config.0.nat_ip}"
            type        = "ssh"
            user        = "${var.ssh_user}"
            timeout     = "1800s"
            private_key = "${file("${local.ssh_private_key}")}"
        }
        inline = [
            <<EOF
            until systemctl --quiet status docker | grep active >/dev/null 2>&1;
            do
                echo 'waiting on docker to start...'
                sleep 5
            done
            until which kubeadm;
            do
                echo 'waiting on kubeadm install...';
                sleep 5; 
            done;
            sudo kubeadm init --pod-network-cidr=${var.pod_network_cidr} --apiserver-advertise-address ${var.apiserver_advertise_address} --token ${var.k8s_token} --apiserver-cert-extra-sans ${join(",",compact(concat(["${google_compute_instance.controller.network_interface.0.access_config.0.nat_ip}"],"${var.apiserver_cert_extra_sans}")))}
            sudo sed -i "s/${google_compute_instance.controller.network_interface.0.network_ip}/${google_compute_instance.controller.network_interface.0.access_config.0.nat_ip}/g" /etc/kubernetes/admin.conf
            mkdir -p ~/.kube
            sudo cp -i /etc/kubernetes/admin.conf ~/.kube/config
            sudo chown $(id -u):$(id -g) $HOME/.kube/config
            until curl -k -q https://127.0.0.1:6443 >/dev/null 2>&1;
            do
                echo 'waiting for Kubernetes API to be accessible...'
                sleep 5
            done
            kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
            EOF
        ]
    }
}

data "external" "kubeconfig" {
  depends_on = ["google_compute_instance.controller"]
  program = ["${path.module}/../files/get_kubeconfig.sh"]
  query = {
    user = "${var.ssh_user}"
    sshkey = "${local.ssh_private_key}"
    controller = "${google_compute_instance.controller.network_interface.0.access_config.0.nat_ip}"
  }
}

resource "local_file" "kubeconfig" {
  depends_on = ["data.external.kubeconfig"]
  content  = "${data.external.kubeconfig.result.kubeconfig == "" ? "/etc/kubernetes/admin.conf was empty" : data.external.kubeconfig.result.kubeconfig}"
  filename = "${local.kubeconfig_path}"
}

data "template_file" "node" {
  template = "${file("${path.module}/../files/node.tpl")}"

  vars = {
    kube_token       = "${var.k8s_token}"
    primary_node_ip  = "${google_compute_instance.controller.network_interface.0.access_config.0.nat_ip}"
    kube_version     = "${var.kubernetes_version}"
    openebs_ready    = "${var.openebs_ready}"
  }
}

resource "google_compute_firewall" "k8s" {
  name    = "k8s-${var.cluster_name}"
  network = "default"

  source_ranges = [
      "${var.apiserver_fw_source_range}"
  ]

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["443", "6443"]
  }
}

resource "google_compute_disk" "node" {
    count = "${var.node_count * var.node_volume_count}"
    name  = "${var.cluster_name}-node-disk-${count.index}"
    type  = "pd-ssd"
    size = "${var.node_volume_size}"
    zone  = "${var.gcp_zone}"
}

resource "google_compute_instance" "node" {
    count = "${var.node_count}"
    name = "k8s-${var.cluster_name}-node-${count.index}"
    machine_type = "${var.gcp_k8s_node_size}"
    zone = "${var.gcp_zone}"
    boot_disk {
        initialize_params {
            image = "${var.gcp_k8s_node_image}"
            size = "${var.gcp_boot_disk_size}"
        }
    }
    metadata = {
        user-data = "${data.template_file.node.rendered}"
        sshKeys = "${var.ssh_user}:${file(var.ssh_key)}"
    }
    network_interface {
        network = "default"
        access_config {}
    }
    service_account {
        scopes = ["userinfo-email", "compute-ro", "storage-ro"]
    }
    dynamic "attached_disk" {
        for_each = [for vol in slice(google_compute_disk.node, (count.index * var.node_volume_count), ((count.index * var.node_volume_count) + (var.node_volume_count))): {
            name = vol.name
            source = vol.self_link
        }]

        content {
            device_name = attached_disk.value.name
            source      = attached_disk.value.source
        }
    }
    provisioner "remote-exec" {
        connection {
            host        = "${self.network_interface.0.access_config.0.nat_ip}"
            type        = "ssh"
            user        = "${var.ssh_user}"
            timeout     = "1800s"
            private_key = "${file("${local.ssh_private_key}")}"
        }
        inline = [
            <<EOF
            until systemctl --quiet status docker | grep active >/dev/null 2>&1;
            do
                echo 'waiting on docker to start on node ${count.index}...'
                sleep 5
            done
            until which kubeadm;
            do
                echo 'waiting on kubeadm install on ${count.index}...';
                sleep 5; 
            done;
            echo "Attempting to join cluster"
            until curl -k -q https://${google_compute_instance.controller.network_interface.0.access_config.0.nat_ip}:6443 >/dev/null 2>&1;
            do
                echo 'waiting for Kubernetes API to be accessible...'
                sleep 5
            done
            echo "Joining Kubernetes cluster ${google_compute_instance.controller.network_interface.0.access_config.0.nat_ip}:6443..."
            sudo kubeadm join "${google_compute_instance.controller.network_interface.0.access_config.0.nat_ip}:6443" --token "${var.k8s_token}" --discovery-token-unsafe-skip-ca-verification

            if [ "${var.k8s_wait_for_nodes}" = "true" ];
            then
                until curl -s http://localhost:10248/healthz | grep ok;
                do
                    echo "Waiting for node ${count.index} to be ready..."
                    sleep 10
                done
            else
                echo "Not waiting for nodes to be in Ready state."
            fi
            EOF
        ]
    }
}