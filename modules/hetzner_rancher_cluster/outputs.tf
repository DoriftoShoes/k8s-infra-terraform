output "rancher-url" {
  value =["https://${var.floating_ip == "none-at-all" ? hcloud_floating_ip.rancherserver.0.ip_address : data.hcloud_floating_ip.rancherserver.0.ip_address}"]
}