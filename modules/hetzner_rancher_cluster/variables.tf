variable "hcloud_token" {
  default = "CHANGEME"
}

variable "prefix" {
  default = "yourname"
}

variable "rancherserver_url" {}

variable "ssh_key" {
  default = "CHANGEME"
}

variable "ssh_user" {
  default = "CHANGEME"
}

variable "ssh_key_name" {
  default = "none-at-all"
}

variable "rancher_version" {
  default = "latest"
}

variable "docker_version_server" {
  default = "18.09"
}

variable "docker_version_agent" {
  default = "18.09"
}

variable "count_agent_all_nodes" {
  default = "3"
}

variable "count_agent_etcd_nodes" {
  default = "0"
}

variable "count_agent_controlplane_nodes" {
  default = "0"
}

variable "count_agent_worker_nodes" {
  default = "0"
}

variable "admin_password" {
  default = "admin"
}

variable "cluster_name" {}

variable "node_count" {
    default = 3
}

variable "node_volume_count" {
    default = 2
}

variable "node_volume_size" {
    default = 100
}

variable "floating_ip" {
  default = "none-at-all"
}

variable "hetzner_image" {
    default = "ubuntu-18.04"
}

variable "hetzner_location" {
    default = "nbg1"
}

variable "hetzner_rancherserver_type" {
  default = "cx41"
}

variable "hetzner_rancheragent-all_type" {
  default = "cx51"
}

variable "hetzner_rancheragent-controlplane_type" {
  default = "cx41"
}

variable "hetzner_rancheragent-worker_type" {
  default = "cx51"
}

variable "hetzner_rancheragent-etcd_type" {
  default = "cx41"
}