provider "hcloud" {                # Provider designation hetzner
  token = "${var.hcloud_token}"      # The token for connection to the hetzner API is specified in the terraform.tfvars file
}

locals {
    ssh_private_key = "${element(split(".pub","${var.ssh_key}"),0)}"
}

data "hcloud_ssh_key" "ssh_key" {
  count = "${var.ssh_key_name == "none-at-all" ? 0 : 1}"
  name = "${var.prefix}-${var.cluster_name}-${var.ssh_key_name}"
}

data "hcloud_floating_ip" "rancherserver" {
  count = "${var.floating_ip == "none-at-all" ? 0 : 1}"
  ip_address = "${var.floating_ip}"
}

data "template_file" "userdata_server" {
  template = "${file("${path.module}/../files/rancher_userdata_server")}"

  vars = {
    cloud_provider        = "HETZNER"
    admin_password        = "${var.admin_password}"
    cluster_name          = "${var.prefix}-${var.cluster_name}"
    docker_version_server = "${var.docker_version_server}"
    rancher_version       = "${var.rancher_version}"
    rancherserver_url     = "${var.rancherserver_url}"
  }
}

data "template_file" "userdata_agent" {
  template = "${file("${path.module}/../files/rancher_userdata_agent")}"

  vars = {
    cloud_provider        = "HETZNER"
    admin_password       = "${var.admin_password}"
    cluster_name         = "${var.prefix}-${var.cluster_name}"
    docker_version_agent = "${var.docker_version_agent}"
    rancher_version      = "${var.rancher_version}"
    #server_address       = "${hcloud_server.rancherserver.ipv4_address}"
    server_address       = "${var.rancherserver_url}"
  }
}

resource "hcloud_ssh_key" "user" {
    count = "${var.ssh_key_name == "none-at-all" ? 1 : 0}"
    name = "${var.ssh_user}"
    public_key = "${file(var.ssh_key)}"
}

resource "hcloud_server" "rancherserver" {
  name = "${var.prefix}-${var.cluster_name}-rancherserver"
  image = "${var.hetzner_image}"
  server_type = "${var.hetzner_rancherserver_type}"
  location = "${var.hetzner_location}"
  backups = "false"
  ssh_keys = ["${var.ssh_key_name == "none-at-all" ? hcloud_ssh_key.user.0.id : data.hcloud_ssh_key.ssh_key.0.id}"]
  user_data = "${data.template_file.userdata_server.rendered}"

  provisioner "file" {
    connection {
            host        = "${self.ipv4_address}"
            type        = "ssh"
            user        = "root"
            timeout     = "1800s"
            private_key = "${file("${local.ssh_private_key}")}"
        }
    content     = "${data.template_file.rancherserver_floating_ip.rendered}"
    destination = "${var.floating_ip == "none-at-all" ? "/etc/network/interfaces.d/60-${hcloud_floating_ip.rancherserver.0.ip_address}.cfg" : "/etc/network/interfaces.d/60-${data.hcloud_floating_ip.rancherserver.0.ip_address}.cfg"}"
  }

  provisioner "remote-exec" {
    connection {
            host        = "${self.ipv4_address}"
            type        = "ssh"
            user        = "root"
            timeout     = "1800s"
            private_key = "${file("${local.ssh_private_key}")}"
        }
    inline = [
      "service networking restart"
    ]
    on_failure = "continue"
  }
}

resource "hcloud_floating_ip" "rancherserver" {
  count = "${var.floating_ip == "none-at-all" ? 1 : 0}"
  type = "ipv4"
}

resource "hcloud_floating_ip_assignment" "rancherserver" {
  floating_ip_id = "${var.floating_ip == "none-at-all" ? hcloud_floating_ip.rancherserver.0.id : data.hcloud_floating_ip.rancherserver.0.id}"
  server_id = "${hcloud_server.rancherserver.id}"
}

data "template_file" "rancherserver_floating_ip" {
  template = "${file("${path.module}/../files/hetzner_floating_ip.tpl")}"

  vars = {
    floating_ip = "${var.floating_ip == "none-at-all" ? "${hcloud_floating_ip.rancherserver.0.ip_address}" : "${data.hcloud_floating_ip.rancherserver.0.ip_address}"}"
  }
}

resource "hcloud_server" "rancheragent-all" {
  count = "${var.count_agent_all_nodes}"
  name = "${var.prefix}-${var.cluster_name}-rancheragent-${count.index}-all"
  image = "${var.hetzner_image}"
  server_type = "${var.hetzner_rancheragent-all_type}"
  location = "${var.hetzner_location}"
  backups = "false"
  ssh_keys = ["${var.ssh_key_name == "none-at-all" ? hcloud_ssh_key.user.0.id : data.hcloud_ssh_key.ssh_key.0.id}"]
  user_data = "${data.template_file.userdata_agent.rendered}"
}

resource "hcloud_volume" "all" {
  count = "${var.count_agent_all_nodes * var.node_volume_count}"
  name = "${var.prefix}-${var.cluster_name}-rancheragent-all-${count.index}"
  size = "${var.node_volume_size}"
  server_id = "${element(hcloud_server.rancheragent-all.*.id,count.index / var.count_agent_all_nodes)}"
  automount = false
}

resource "hcloud_server" "rancheragent-etcd" {
  count = "${var.count_agent_etcd_nodes}"
  name = "${var.prefix}-${var.cluster_name}-rancheragent-${count.index}-etcd"
  image = "${var.hetzner_image}"
  server_type = "${var.hetzner_rancheragent-etcd_type}"
  location = "${var.hetzner_location}"
  backups = "false"
  ssh_keys = ["${var.ssh_key_name == "none-at-all" ? hcloud_ssh_key.user.0.id : data.hcloud_ssh_key.ssh_key.0.id}"]
  user_data = "${data.template_file.userdata_agent.rendered}"
}

resource "hcloud_server" "rancheragent-controlplane" {
  count = "${var.count_agent_controlplane_nodes}"
  name = "${var.prefix}-${var.cluster_name}-rancheragent-${count.index}-controlplane"
  image = "${var.hetzner_image}"
  server_type = "${var.hetzner_rancheragent-controlplane_type}"
  location = "${var.hetzner_location}"
  backups = "false"
  ssh_keys = ["${var.ssh_key_name == "none-at-all" ? hcloud_ssh_key.user.0.id : data.hcloud_ssh_key.ssh_key.0.id}"]
  user_data = "${data.template_file.userdata_agent.rendered}"
}

resource "hcloud_server" "rancheragent-worker" {
  count = "${var.count_agent_worker_nodes}"
  name = "${var.prefix}-${var.cluster_name}-rancheragent-${count.index}-worker"
  image = "${var.hetzner_image}"
  server_type = "${var.hetzner_rancheragent-worker_type}"
  location = "${var.hetzner_location}"
  backups = "false"
  ssh_keys = ["${var.ssh_key_name == "none-at-all" ? hcloud_ssh_key.user.0.id : data.hcloud_ssh_key.ssh_key.0.id}"]
  user_data = "${data.template_file.userdata_agent.rendered}"
}

resource "hcloud_volume" "worker-a" {
  count = "${var.count_agent_worker_nodes}"
  name = "${var.prefix}-${var.cluster_name}-rancheragent-worker-${count.index}-vol-a"
  size = "${var.node_volume_size}"
  location = "${var.hetzner_location}"
  automount = false
}

resource "hcloud_volume_attachment" "worker-a" {
  count = "${var.count_agent_worker_nodes}"
  volume_id = "${element(hcloud_volume.worker-a.*.id, count.index)}"
  server_id = "${element(hcloud_server.rancheragent-worker.*.id, count.index)}"
  automount = false
}

#resource "hcloud_volume" "worker-b" {
#  count = "${var.count_agent_worker_nodes}"
#  name = "${var.prefix}-${var.cluster_name}-rancheragent-worker-${count.index}-vol-b"
#  size = "${var.node_volume_size}"
#  location = "${var.hetzner_location}"
#  automount = false
#}

#resource "hcloud_volume_attachment" "worker-b" {
#  count = "${var.count_agent_worker_nodes}"
#  volume_id = "${element(hcloud_volume.worker-b.*.id, count.index)}"
#  server_id = "${element(hcloud_server.rancheragent-worker.*.id, count.index)}"
#  automount = false
#}
