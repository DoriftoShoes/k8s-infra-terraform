variable "digitalocean_token" {
  description = "DO Token"
}

variable "k8s_token" {
    default = "thisisatokenyoushouldchangeit"
}

variable "prefix" {
    default = "dummy"
}

variable "do_region" {
  description = "DigitalOcean Region"
  default     = "sfo2"
}

variable "do_image" {
    default = "ubuntu-19-04-x64"
}

variable "do_node_size" {
  description = "K8s Node Size"
  default     = "s-4vcpu-8gb"
}

variable "do_controller_size" {
  description = "K8s Controller Size"
  default     = "s-2vcpu-4gb"
}

variable "cluster_name" {
  default     = "digitalocean-k8s"
}

variable "do_node_count" {
  default     = "3"
  description = "Number of nodes."
}

variable "node_volume_count" {
    default = 2
}

variable "node_volume_size" {
    default   = 100
}

variable "do_ssh_keys" {
  description = "ssh key ids"
  type        = "list"
}

variable "do_ssh_private_key" {
  description = "local path to private key"
  default = ""
}

variable "kubernetes_version" {
  description = "Version of Kubeadm to install"
  default     = "1.14.2-00"
}

variable "openebs_ready" {
    default = "1"
}

variable "pod_network_cidr" {
    default = "10.244.0.0/16"
}

variable "apiserver_advertise_address" {
    default = "0.0.0.0"
}

variable "apiserver_fw_source_range" {
    default = "0.0.0.0/0"
}

variable "apiserver_cert_extra_sans" {
    default = [""]
    type = list(string)
}

variable "kubeconfig_path" {
    default = ""
}

variable "k8s_wait_for_nodes" {
    default = "true"
}