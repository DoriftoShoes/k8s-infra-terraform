locals {
    kubeconfig_path = "${var.kubeconfig_path != "" ? var.kubeconfig_path : "kubeconfig_do_${var.cluster_name}"}"
}

provider "digitalocean" {
  token = "${var.digitalocean_token}"
}

data "template_file" "controller" {
  template = "${file("${path.module}/../files/controller.tpl")}"

  vars = {
    kube_version       = "${var.kubernetes_version}"
  }
}

resource "digitalocean_droplet" "controller" {
  name               = "${var.prefix}-${var.cluster_name}-controller"
  image              = "${var.do_image}"
  size               = "${var.do_controller_size}"
  region             = "${var.do_region}"
  backups            = "true"
  private_networking = "true"
  ssh_keys           = "${var.do_ssh_keys}"
  user_data          = "${data.template_file.controller.rendered}"

  provisioner "remote-exec" {
        connection {
            host        = "${digitalocean_droplet.controller.ipv4_address}"
            type        = "ssh"
            user        = "root"
            timeout     = "1800s"
            private_key = "${file("${var.do_ssh_private_key}")}"
        }
        inline = [
            <<EOF
            until systemctl --quiet status docker | grep active >/dev/null 2>&1;
            do
                echo 'waiting on docker to start...'
                sleep 5
            done
            until which kubeadm;
            do
                echo 'waiting on kubeadm install...';
                sleep 5; 
            done;
            sudo kubeadm init --pod-network-cidr=${var.pod_network_cidr} --apiserver-advertise-address ${var.apiserver_advertise_address} --token ${var.k8s_token} --apiserver-cert-extra-sans ${join(",",compact(concat(["${digitalocean_droplet.controller.ipv4_address}"],"${var.apiserver_cert_extra_sans}")))}
            mkdir -p ~/.kube
            sudo cp -i /etc/kubernetes/admin.conf ~/.kube/config
            sudo chown $(id -u):$(id -g) $HOME/.kube/config
            until curl -k -q https://127.0.0.1:6443 >/dev/null 2>&1;
            do
                echo 'waiting for Kubernetes API to be accessible...'
                sleep 5
            done
            kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
            EOF
        ]
    }
}

data "external" "kubeconfig" {
  depends_on = ["digitalocean_droplet.controller"]
  program = ["${path.module}/../files/get_kubeconfig.sh"]
  query = {
    user = "root"
    sshkey = "${var.do_ssh_private_key}"
    controller = "${digitalocean_droplet.controller.ipv4_address}"
  }
}

resource "local_file" "kubeconfig" {
  depends_on = ["data.external.kubeconfig"]
  content  = "${data.external.kubeconfig.result.kubeconfig == "" ? "/etc/kubernetes/admin.conf was empty" : data.external.kubeconfig.result.kubeconfig}"
  filename = "${local.kubeconfig_path}"
}

resource "digitalocean_volume" "node" {
  count                   = "${var.do_node_count * var.node_volume_count}"
  region                  = "${var.do_region}"
  name                    = "${var.prefix}-node-${count.index}"
  size                    = "${var.node_volume_size}"
  description             = "node volume ${count.index}"
}

data "template_file" "node" {
  template = "${file("${path.module}/../files/node.tpl")}"

  vars = {
    kube_version    = "${var.kubernetes_version}"
    openebs_ready   = "${var.openebs_ready}"
  }
}

resource "digitalocean_droplet" "node" {
  name               = "${var.prefix}-${format("${var.cluster_name}-node-%02d", count.index)}"
  image              = "${var.do_image}"
  count              = "${var.do_node_count}"
  size               = "${var.do_node_size}"
  region             = "${var.do_region}"
  private_networking = "true"
  ssh_keys           = "${var.do_ssh_keys}"
  user_data          = "${data.template_file.node.rendered}"
  volume_ids = [
    for vol in slice(digitalocean_volume.node, (count.index * var.node_volume_count), ((count.index * var.node_volume_count) + (var.node_volume_count))):
    vol.id
  ]

  provisioner "remote-exec" {
        connection {
            host        = "${self.ipv4_address}"
            type        = "ssh"
            user        = "root"
            timeout     = "1800s"
            private_key = "${file("${var.do_ssh_private_key}")}"
        }
        inline = [
            <<EOF
            until systemctl --quiet status docker | grep active >/dev/null 2>&1;
            do
                echo 'waiting on docker to start on node ${count.index}...'
                sleep 5
            done
            until which kubeadm;
            do
                echo 'waiting on kubeadm install on ${count.index}...';
                sleep 5; 
            done;
            echo "Attempting to join cluster"
            until curl -k -q https://${digitalocean_droplet.controller.ipv4_address}:6443 >/dev/null 2>&1;
            do
                echo 'waiting for Kubernetes API to be accessible...'
                sleep 5
            done
            echo "Joining Kubernetes cluster ${digitalocean_droplet.controller.ipv4_address}:6443..."
            sudo kubeadm join "${digitalocean_droplet.controller.ipv4_address}:6443" --token "${var.k8s_token}" --discovery-token-unsafe-skip-ca-verification

            if [ "${var.k8s_wait_for_nodes}" = "true" ];
            then
                until curl -s http://localhost:10248/healthz | grep ok;
                do
                    echo "Waiting for node ${count.index} to be ready..."
                    sleep 10
                done
            else
                echo "Not waiting for nodes to be in Ready state."
            fi
            EOF
        ]
    }
}