variable "gcp_credentials_file" {
    default = "gcloud_credentials.json"
}

variable "gcp_project_id" {
    default = "mayadata-sample"
}

variable "prefix" {
  default = "yourname"
}

variable "rancher_version" {
  default = "latest"
}

variable "count_agent_all_nodes" {
  default = "3"
}

variable "count_agent_etcd_nodes" {
  default = "0"
}

variable "count_agent_controlplane_nodes" {
  default = "0"
}

variable "count_agent_worker_nodes" {
  default = "0"
}

variable "admin_password" {
  default = "admin"
}

variable "cluster_name" {}

variable "gcp_region" {
  default = "us-central1"
}

variable "gcp_zone" {
    default = "us-central1-a"
}

variable "node_count" {
    default = 3
}

variable "node_volume_count" {
    default = 2
}

variable "node_volume_size" {
    default = 100
}

variable "gcp_rancher_image" {
    default = "ubuntu-1804-lts"
}

variable "gcp_rancher_server_size" {
  default = "n1-highcpu-4"
}

variable "gcp_rancher_all_size" {
  default = "n1-highcpu-4"
}

variable "gcp_rancher_controlplane_size" {
  default = "n1-highcpu-4"
}

variable "gcp_rancher_worker_size" {
  default = "n1-highcpu-4"
}

variable "gcp_rancher_etcd_size" {
  default = "n1-highcpu-4"
}

variable "gcp_boot_disk_size" {
    default = 20
}

variable "apiserver_fw_source_range" {
    default = "0.0.0.0/0"
}

variable "docker_version_server" {
  default = "18.09"
}

variable "docker_version_agent" {
  default = "18.09"
}

variable "ssh_user" {
    default = "sampleuser"
}

variable "ssh_key" {
  default = []
}

variable "openebs_ready" {
  default = 1
}