locals {
    ssh_private_key = "${element(split(".pub","${var.ssh_key}"),0)}"
}

provider "google" {
    credentials = "${file("${var.gcp_credentials_file}")}"
    project = "${var.gcp_project_id}"
    region = "${var.gcp_region}"
}

data "template_file" "userdata_server" {
  template = "${file("${path.module}/../files/rancher_userdata_server")}"

  vars = {
    cloud_provider        = "GCE"
    admin_password        = "${var.admin_password}"
    cluster_name          = "${var.cluster_name}"
    docker_version_server = "${var.docker_version_server}"
    rancher_version       = "${var.rancher_version}"
  }
}

data "template_file" "userdata_agent" {
  template = "${file("${path.module}/../files/rancher_userdata_agent")}"

  vars = {
    cloud_provider        = "GCE"
    admin_password       = "${var.admin_password}"
    cluster_name         = "${var.cluster_name}"
    docker_version_agent = "${var.docker_version_agent}"
    rancher_version      = "${var.rancher_version}"
    server_address       = "${google_compute_instance.rancherserver.network_interface.0.access_config.0.nat_ip}"
    openebs_ready        = "${var.openebs_ready}"
  }
}

resource "google_compute_instance" "rancherserver" {
    name = "${var.prefix}-rancherserver"
    machine_type = "${var.gcp_rancher_server_size}"
    zone = "${var.gcp_zone}"
    boot_disk {
        initialize_params {
            image = "${var.gcp_rancher_image}"
            size = "${var.gcp_boot_disk_size}"
        }
    }
    metadata = {
        user-data = "${data.template_file.userdata_server.rendered}"
        sshKeys = "${var.ssh_user}:${file(var.ssh_key)}"
    }
    network_interface {
        network = "default"
        access_config {}
    }
    service_account {
        scopes = ["userinfo-email", "compute-ro", "storage-ro"]
    }
}

resource "google_compute_disk" "node" {
    count = "${var.node_count * var.node_volume_count}"
    name  = "${var.cluster_name}-node-disk-${count.index}"
    type  = "pd-ssd"
    size = "${var.node_volume_size}"
    zone  = "${var.gcp_zone}"
}

resource "google_compute_instance" "rancheragent-all" {
    count = "${var.count_agent_all_nodes}"
    name = "${var.prefix}-rancheragent-${count.index}-all"
    machine_type = "${var.gcp_rancher_all_size}"
    zone = "${var.gcp_zone}"
    boot_disk {
        initialize_params {
            image = "${var.gcp_rancher_image}"
            size = "${var.gcp_boot_disk_size}"
        }
    }
    metadata = {
        user-data = "${data.template_file.userdata_agent.rendered}"
        sshKeys = "${var.ssh_user}:${file(var.ssh_key)}"
    }
    network_interface {
        network = "default"
        access_config {}
    }
    service_account {
        scopes = ["userinfo-email", "compute-ro", "storage-ro"]
    }
    dynamic "attached_disk" {
        for_each = [for vol in slice(google_compute_disk.node, (count.index * var.node_volume_count), ((count.index * var.node_volume_count) + (var.node_volume_count))): {
            name = vol.name
            source = vol.self_link
        }]

        content {
            device_name = attached_disk.value.name
            source      = attached_disk.value.source
        }
    }
}

resource "google_compute_instance" "rancheragent-etcd" {
    count = "${var.count_agent_etcd_nodes}"
    name = "${var.prefix}-rancheragent-${count.index}-etcd"
    machine_type = "${var.gcp_rancher_etcd_size}"
    zone = "${var.gcp_zone}"
    boot_disk {
        initialize_params {
            image = "${var.gcp_rancher_image}"
            size = "${var.gcp_boot_disk_size}"
        }
    }
    metadata = {
        user-data = "${data.template_file.userdata_agent.rendered}"
        sshKeys = "${var.ssh_user}:${file(var.ssh_key)}"
    }
    network_interface {
        network = "default"
        access_config {}
    }
    service_account {
        scopes = ["userinfo-email", "compute-ro", "storage-ro"]
    }
}

resource "google_compute_instance" "rancheragent-controlplane" {
    count = "${var.count_agent_controlplane_nodes}"
    name = "${var.prefix}-rancheragent-${count.index}-controlplane"
    machine_type = "${var.gcp_rancher_controlplane_size}"
    zone = "${var.gcp_zone}"
    boot_disk {
        initialize_params {
            image = "${var.gcp_rancher_image}"
            size = "${var.gcp_boot_disk_size}"
        }
    }
    metadata = {
        user-data = "${data.template_file.userdata_agent.rendered}"
        sshKeys = "${var.ssh_user}:${file(var.ssh_key)}"
    }
    network_interface {
        network = "default"
        access_config {}
    }
    service_account {
        scopes = ["userinfo-email", "compute-ro", "storage-ro"]
    }
}

resource "google_compute_instance" "rancheragent-worker" {
    count = "${var.count_agent_worker_nodes}"
    name = "${var.prefix}-rancheragent-${count.index}-worker"
    machine_type = "${var.gcp_rancher_worker_size}"
    zone = "${var.gcp_zone}"
    boot_disk {
        initialize_params {
            image = "${var.gcp_rancher_image}"
            size = "${var.gcp_boot_disk_size}"
        }
    }
    metadata = {
        user-data = "${data.template_file.userdata_agent.rendered}"
        sshKeys = "${var.ssh_user}:${file(var.ssh_key)}"
    }
    network_interface {
        network = "default"
        access_config {}
    }
    service_account {
        scopes = ["userinfo-email", "compute-ro", "storage-ro"]
    }
    dynamic "attached_disk" {
        for_each = [for vol in slice(google_compute_disk.node, (count.index * var.node_volume_count), ((count.index * var.node_volume_count) + (var.node_volume_count))): {
            source = vol.self_link
        }]

        content {
            source      = attached_disk.value.source
        }
    }
}

resource "google_compute_firewall" "k8s" {
  name    = "k8s-${var.cluster_name}"
  network = "default"

  source_ranges = [
      "${var.apiserver_fw_source_range}"
  ]

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["443", "6443"]
  }
}