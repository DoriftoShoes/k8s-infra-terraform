locals {
    ssh_private_key = "${element(split(".pub","${var.ssh_key}"),0)}"
    kubeconfig_path = "${var.kubeconfig_path != "" ? var.kubeconfig_path_aws : "kubeconfig_aws_${var.prefix}_${var.cluster_name}"}"
}

provider "aws" {
  region                  = "${var.aws_region}"
  shared_credentials_file = "${var.aws_shared_credentials_file}"
  profile                 = "${var.aws_profile}"
}

resource "aws_vpc" "k8s_cluster" {
  cidr_block = "${var.aws_k8s_vpc_cidr}"

  tags = {
    Name = "vpc-${var.prefix}-${var.cluster_name}"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.k8s_cluster.id}"
  tags = {
    Name = "igw-${var.prefix}-${var.cluster_name}"
  }
}

resource "aws_eip" "gw_eip" {
  vpc      = true
  depends_on = ["aws_internet_gateway.igw"]
}

resource "aws_nat_gateway" "gw" {
  allocation_id = "${aws_eip.gw_eip.id}"
  subnet_id     = "${aws_subnet.subnet_public.id}"
  depends_on = ["aws_internet_gateway.igw"]
}

resource "aws_route_table" "rt_private" {
    vpc_id = "${aws_vpc.k8s_cluster.id}"

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = "${aws_nat_gateway.gw.id}"
    }

    tags = {
        Name = "route_table-${var.prefix}-${var.cluster_name}"
    }
}

resource "aws_subnet" "subnet_public" {
  vpc_id = "${aws_vpc.k8s_cluster.id}"
  cidr_block = "${var.aws_k8s_cidr_public_subnet}"
  map_public_ip_on_launch = "true"
  availability_zone = "${var.aws_zones.0}"
  tags = {
    Name = "public-subnet-${var.prefix}-${var.cluster_name}"
  }
}

resource "aws_subnet" "subnet_private" {
  count = "${length(var.aws_k8s_cidr_private_subnets)}"
  vpc_id = "${aws_vpc.k8s_cluster.id}"
  cidr_block = "${element(var.aws_k8s_cidr_private_subnets,count.index)}"
  availability_zone = "${element(var.aws_zones, count.index)}"
  tags = {
    Name = "private-subnet-${var.prefix}-${var.cluster_name}"
  }
}

resource "aws_route" "public" {
  route_table_id            = "${aws_vpc.k8s_cluster.main_route_table_id}"
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = "${aws_internet_gateway.igw.id}"
}

resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = "${aws_subnet.subnet_public.id}"
  route_table_id = "${aws_vpc.k8s_cluster.main_route_table_id}"
}

resource "aws_route_table_association" "rta_subnet_private" {
  count = "${length(var.aws_k8s_cidr_private_subnets)}"
  subnet_id      = "${element(aws_subnet.subnet_private.*.id,count.index)}"
  route_table_id = "${aws_route_table.rt_private.id}"
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "template_cloudinit_config" "controller-cloudinit" {
  part {
    content_type = "text/cloud-config"
    content      = "hostname: ${var.prefix}-${var.cluster_name}-controller\nmanage_etc_hosts: true"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.userdata_controller.rendered}"
  }
}



data "template_file" "userdata_controller" {
  template = "${file("${path.module}/../files/controller.tpl")}"

  vars = {
    kube_version       = "${var.kubernetes_version}"
  }
}

data "template_cloudinit_config" "node-cloudinit" {
  count = "${var.aws_node_count}"
  part {
    content_type = "text/cloud-config"
    content      = "hostname: ${var.prefix}-${var.cluster_name}-node-${count.index}\nmanage_etc_hosts: true"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.userdata_node.rendered}"
  }
}

data "template_file" "userdata_node" {
  template = "${file("${path.module}/../files/node.tpl")}"

  vars = {
    kube_version    = "${var.kubernetes_version}"
    openebs_ready   = "${var.openebs_ready}"
  }
}

resource "aws_instance" "controller" {
    ami           = "${data.aws_ami.ubuntu.id}"
    instance_type = "${var.aws_k8s_controller_size}"
    subnet_id = "${aws_subnet.subnet_public.id}"
    availability_zone = "${aws_subnet.subnet_public.availability_zone}"
    tags = {
        Name = "${var.prefix}-${var.cluster_name}-controller"
    }
    user_data     = "${data.template_cloudinit_config.controller-cloudinit.rendered}"
    key_name      = "${var.aws_key_name}"
    security_groups = ["${aws_security_group.controller.id}"]

    provisioner "remote-exec" {
        connection {
            host        = "${aws_instance.controller.public_ip}"
            type        = "ssh"
            user        = "ubuntu"
            timeout     = "1800s"
            private_key = "${file("${local.ssh_private_key}")}"
        }
        inline = [
            <<EOF
            until systemctl --quiet status docker | grep active >/dev/null 2>&1;
            do
                echo 'waiting on docker to start...'
                sleep 5
            done
            until which kubeadm;
            do
                echo 'waiting on kubeadm install...';
                sleep 5; 
            done;
            sudo kubeadm init --pod-network-cidr=${var.pod_network_cidr} --apiserver-advertise-address ${var.apiserver_advertise_address} --token ${var.k8s_token} --apiserver-cert-extra-sans ${join(",",compact(concat(["${aws_instance.controller.public_ip}"],"${var.apiserver_cert_extra_sans}")))}
            sudo sed -i "s/${aws_instance.controller.private_ip}/${aws_instance.controller.public_ip}/g" /etc/kubernetes/admin.conf
            mkdir -p ~/.kube
            sudo cp -i /etc/kubernetes/admin.conf ~/.kube/config
            sudo chown $(id -u):$(id -g) $HOME/.kube/config
            until curl -k -q https://127.0.0.1:6443 >/dev/null 2>&1;
            do
                echo 'waiting for Kubernetes API to be accessible...'
                sleep 5
            done
            kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
            EOF
        ]
    }
}

data "external" "kubeconfig" {
  depends_on = ["aws_instance.controller"]
  program = ["${path.module}/../files/get_kubeconfig.sh"]
  query = {
    user = "ubuntu"
    sshkey = "${local.ssh_private_key}"
    controller = "${aws_instance.controller.public_ip}"
  }
}

resource "local_file" "kubeconfig" {
  depends_on = ["data.external.kubeconfig"]
  content  = "${data.external.kubeconfig.result.kubeconfig == "" ? "/etc/kubernetes/admin.conf was empty" : data.external.kubeconfig.result.kubeconfig}"
  filename = "${local.kubeconfig_path}"
}

resource "random_string" "device_name" {
    count = "${var.node_volume_count}"
    length = 1
    number = false
    upper = false
    special = false
}

resource "aws_instance" "k8s_node" {
    count             = "${var.aws_node_count}"
    ami               = "${data.aws_ami.ubuntu.id}"
    instance_type     = "${var.aws_k8s_node_size}"
    subnet_id = "${element(aws_subnet.subnet_private.*.id, count.index)}"
    availability_zone = "${element(aws_subnet.subnet_private.*.availability_zone, count.index)}"
    tags = {
        Name = "${var.prefix}-${var.cluster_name}-node-${count.index}"
    }
    user_data         = "${element(data.template_cloudinit_config.node-cloudinit.*.rendered,count.index)}"
    key_name          = "${var.aws_key_name}"
    security_groups = ["${aws_security_group.k8s-main.id}"]

    dynamic "ebs_block_device" {
        for_each = [for name in random_string.device_name: {
            device_name = name.result
        }]

        content {
            device_name = "/dev/xvd${ebs_block_device.value.device_name}"
            volume_size = "${var.node_volume_size}"
            volume_type = "${var.aws_node_volume_type}"
        }       
    }

    provisioner "remote-exec" {
        connection {
            host        = "${self.private_ip}"
            type        = "ssh"
            user        = "ubuntu"
            timeout     = "1800s"
            private_key = "${file("${local.ssh_private_key}")}"
            bastion_host        = "${aws_instance.controller.public_ip}"
            bastion_private_key = "${file("${local.ssh_private_key}")}"
        }
        inline = [
            <<EOF
            until systemctl --quiet status docker | grep active >/dev/null 2>&1;
            do
                echo 'waiting on docker to start on node ${count.index}...'
                sleep 5
            done
            until which kubeadm;
            do
                echo 'waiting on kubeadm install on ${count.index}...';
                sleep 5; 
            done;
            echo "Attempting to join cluster"
            until curl -k -q https://${aws_instance.controller.public_ip}:6443 >/dev/null 2>&1;
            do
                echo 'waiting for Kubernetes API to be accessible...'
                sleep 5
            done
            echo "Joining Kubernetes cluster ${aws_instance.controller.public_ip}:6443..."
            sudo kubeadm join "${aws_instance.controller.public_ip}:6443" --token "${var.k8s_token}" --discovery-token-unsafe-skip-ca-verification

            if [ "${var.k8s_wait_for_nodes}" = "true" ];
            then
                until curl -s http://localhost:10248/healthz | grep ok;
                do
                    echo "Waiting for node ${count.index} to be ready..."
                    sleep 10
                done
            else
                echo "Not waiting for nodes to be in Ready state."
            fi
            EOF
        ]
    }
}

resource "aws_security_group" "k8s-main" {
  name = "${var.prefix}-${var.cluster_name}-k8s-main"
  description = "main SG"
  vpc_id = "${aws_vpc.k8s_cluster.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "allow_all_internally" {
  type            = "ingress"
  from_port       = 0
  to_port         = 65535
  protocol        = "-1"
  source_security_group_id = "${aws_security_group.k8s-main.id}"
  security_group_id = "${aws_security_group.k8s-main.id}"
}

resource "aws_security_group" "controller" {
  name        = "${var.prefix}-${var.cluster_name}-controller"
  description = "k8s-controller"
  vpc_id = "${aws_vpc.k8s_cluster.id}"

  ingress {
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}