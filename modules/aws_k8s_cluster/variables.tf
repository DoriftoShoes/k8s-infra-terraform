variable "prefix" {}
variable "cluster_name" {}
variable "aws_k8s_vpc_cidr" {}
variable "aws_k8s_cidr_public_subnet" {}
variable "aws_k8s_cidr_private_subnets" {
  type = list(string)
}
variable "aws_region" {}
variable "aws_zones" {
    type = list(string)
}
variable "aws_shared_credentials_file" {}
variable "aws_profile" {}
variable "aws_node_volume_type" {}
variable "aws_key_name" {}
variable "aws_node_count" {
    default = 3
}
variable "aws_k8s_node_size" {}
variable "aws_k8s_controller_size" {}
variable "openebs_ready" {}
variable "node_volume_count" {}
variable "node_volume_size" {}
variable "kubernetes_version" {}
variable "kubeconfig_path" {}
variable "ssh_key" {}
variable "pod_network_cidr" {
  default = "10.244.0.0/16"
}
variable "apiserver_advertise_address" {
  default = "0.0.0.0"
}
variable "apiserver_cert_extra_sans" {
  default = [""]
  type    = list(string)
}
variable "apiserver_fw_source_range" {
  default = "0.0.0.0/0"
}
variable "k8s_token" {}
variable "k8s_wait_for_nodes" {}