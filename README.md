# K8s Environments for Stateful Workloads
Use Terraform to create k8s environments for stateful workloads (worker nodes have additional cloud disks)

## Requirements
* Account with the appropriate Cloud provider (DigitalOcean, GCP, AWS)
* Terraform 0.12.0 or later

## Assumptions
### General
* Nodes are created with public IPs at the moment.  This will be configurable in a future release.

### GCP
* The private key will be the same path and name as `var.ssh_key` minus the `.pub`.
* For k8s, The user running Terraform should have write access to the path you are trying to save the kubeconfig to.
* The `gcp_credentials_file` is in a location that the user running Terraform can access.
* The GCP project already exists.

### AWS
* The `aws_shared_credentials_file` exists and the user running Terraform can access it.
* The `aws_profile` already exists, and has keys, in the `aws_shared_credentials_file`.
* The SSH key associated with `aws_key_name` already exists.

## How
Currently Terraform modules do not support the count parameter so we need to work around it by setting a target.  Alternatively you can leverage these modules in your own Terraform repos by setting the source line to something like this:
```
source = "github.com/DoriftoShoes/k8s-infra-terraform/modules/gce_k8s_cluster"
```

### Lifecycle
1. Copy tfvars file:
    1. ```cp terraform.tfvars.sample terraform.tfvars```
2. Edit `terraform.tfvars` and set all the appropriate values.
3. Run these commands to deploy (change the module to whichever cluster you want to deploy)
    1. ```terraform plan --target=module.gce_k8s_cluster --out=tf.plan```
    2. ```terraform apply tf.plan```
4. Destroy the cluster when you are done
    1. ```terraform destroy --target=module.gce_k8s_cluster --force```

## Thanks

* A big thanks to the Rancher guys as I re-used a fair bit from their [quickstart repo](https://github.com/rancher/quickstart/).
* The get_kubeconfig.sh script is based on a [snippet](https://gist.github.com/irvingpop/968464132ded25a206ced835d50afa6b) by irvingpop
* The controller and node templates are based on [this](https://github.com/jmarhee/cousteau) work by jmarhee