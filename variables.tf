##### Common Variables #####
variable "k8s_token" {
  default = "foofoo.blahblahblahblah"
}

variable "cluster_name" {
  default = "digitalocean-k8s"
}

variable "prefix" {
  default = "dummy"
}

variable "k8s_wait_for_nodes" {
  default = "true"
}

variable "node_count" {
  default = 3
}

variable "node_volume_count" {
  default = 1
}

variable "node_volume_size" {
  default = 50
}

variable "disk_interface" {
  default = "SCSI"
}

variable "kubernetes_version" {
  description = "Version of Kubeadm to install"
  default     = "1.14.2-00"
}

variable "ssh_user" {
  default = "sampleuser"
}

variable "ssh_key" {
  default = ""
}

variable "pod_network_cidr" {
  default = "10.244.0.0/16"
}

variable "apiserver_advertise_address" {
  default = "0.0.0.0"
}

variable "apiserver_cert_extra_sans" {
  default = [""]
  type    = list(string)
}

variable "apiserver_fw_source_range" {
  default = "0.0.0.0/0"
}

variable "openebs_ready" {}

##### DigitalOcean Variables #####

variable "digitalocean_token" {
  description = "DO Token"
}

variable "do_region" {
  description = "DigitalOcean Region"
  default     = "sfo2"
}

variable "do_image" {
  default = "ubuntu-19-04-x64"
}

variable "do_node_size" {
  description = "K8s Node Size"
  default     = "s-4vcpu-8gb"
}

variable "do_controller_size" {
  description = "K8s Controller Size"
  default     = "s-2vcpu-4gb"
}

variable "do_count" {
  default     = "1"
  description = "Number of clusters."
}

variable "do_ssh_keys" {
  description = "ssh key ids"
  type        = list(string)
}

variable "do_ssh_private_key" {
  description = "local path to private key"
}

##### DigitalOcean Rancher Config #####
variable "rancher_admin_password" {
  default = "password"
}

variable "do_rancher_server_size" {
  default = "s-2vcpu-4gb"
}

variable "do_rancher_all_size" {
  default = "s-2vcpu-4gb"
}

variable "do_rancher_controlplane_size" {
  default = "s-2vcpu-4gb"
}

variable "do_rancher_worker_size" {
  default = "s-2vcpu-4gb"
}

variable "do_rancher_etcd_size" {
  default = "s-2vcpu-4gb"
}

##### Rancher Config #####

variable "rancherserver_url" {}

variable "rancher_version" {
  default = "latest"
}

variable "count_agent_all_nodes" {
  default = "0"
}

variable "count_agent_etcd_nodes" {
  default = "1"
}

variable "count_agent_controlplane_nodes" {
  default = "1"
}

variable "docker_version_server" {
  default = "18.09"
}

variable "docker_version_agent" {
  default = "18.09"
}

##### GCP Variables #####
variable "gcp_credentials_file" {
  default = "somefile.json"
}

variable "gcp_project_id" {
  default = "sample-project"
}

variable "gcp_region" {
  default = "us-central-1"
}

variable "gcp_zone" {
  default = "us-central1-a"
}

variable "gcp_boot_disk_size" {
  default = 20
}

##### GCP k8s Variables #####

variable "gcp_k8s_controller_size" {
  default = "n1-highcpu-4"
}

variable "gcp_k8s_controller_image" {
  default = "ubuntu-1804-lts"
}

variable "gcp_k8s_node_size" {
  default = "n1-highcpu-4"
}

variable "gcp_k8s_node_image" {
  default = "ubuntu-1804-lts"
}

##### GCP Rancher Variables #####
variable "gcp_rancher_image" {
  default = "ubuntu-1804-lts"
}

variable "gcp_rancher_server_size" {
  default = "n1-highcpu-4"
}

variable "gcp_rancher_all_size" {
  default = "n1-highcpu-4"
}

variable "gcp_rancher_controlplane_size" {
  default = "n1-highcpu-4"
}

variable "gcp_rancher_worker_size" {
  default = "n1-highcpu-4"
}

variable "gcp_rancher_etcd_size" {
  default = "n1-highcpu-4"
}

##### AWS Variables #####
variable "aws_rancher_vpc_cidr" {}
variable "aws_rancher_cidr_public_subnet" {}
variable "aws_rancher_cidr_private_subnets" {
  type = list(string)
}
variable "aws_region" {}
variable "aws_zones" {
  type = list(string)
}
variable "aws_shared_credentials_file" {}
variable "aws_profile" {}
variable "aws_rancher_server_size" {}
variable "aws_node_volume_type" {}
variable "aws_key_name" {}
variable "aws_rancher_all_size" {}
variable "aws_rancher_controlplane_size" {}
variable "aws_rancher_worker_size" {}
variable "aws_rancher_etcd_size" {}
variable "kubeconfig_path" {}

##### AWS k8s Variables #####
variable "aws_k8s_vpc_cidr" {}
variable "aws_k8s_cidr_public_subnet" {}
variable "aws_k8s_cidr_private_subnets" {
  type = list(string)
}
variable "aws_k8s_node_size" {}
variable "aws_k8s_controller_size" {}

##### Hetzner Cloud Rancher Variables #####
variable "hcloud_token" {
  default = "CHANGEME"
}

variable "hetzner_image" {
    default = "ubuntu-18.04"
}

variable "hetzner_location" {
    default = "nbg1"
}

variable "hetzner_rancherserver_type" {
  default = "cx41"
}

variable "hetzner_rancher_all_type" {
  default = "cx51"
}

variable "hetzner_rancher_controlplane_type" {
  default = "cx41"
}

variable "hetzner_rancher_worker_type" {
  default = "cx51"
}

variable "hetzner_rancher_etcd_type" {
  default = "cx41"
}

variable "floating_ip" {
  default = "none-at-all"
}

variable "ssh_key_name" {
  default = "none-at-all"
}

